import { useRouter } from "next/router";
import { useEffect, useMemo, useState } from "react";
import repositories from "lib/repositories";
import Picture from "./picture";

export default function InstallationHeader() {
  const router = useRouter();
  const currentOs = useMemo(() => router.asPath.split("/")[2] || "", [router]);
  const [os, setOs] = useState(currentOs);
  const repo = useMemo(
    () => repositories.find((repo) => repo.slug === os),
    [os]
  );

  useEffect(() => {
    setOs((os) => os);
  }, []);

  useEffect(() => {
    if (os !== currentOs) router.push(`/installation/${os}`);
  }, [currentOs, os, router]);

  return (
    <div className="header">
      <h1 className="select">
        Installation instructions for{" "}
        <div className="repo">
          {repo && (
            <>
              <Picture
                src={repo.logo}
                alt={`${repo.name} Logo`}
                className="hide-dark"
              />
              <Picture
                src={repo.logoDark || repo.logo}
                alt={`${repo.name} Logo`}
                className="hide-light"
              />
            </>
          )}
          <select
            value={os}
            onChange={(event) => setOs(event.target.value)}
            className="focus:ring"
          >
            <option value="" disabled hidden>
              ...
            </option>
            {repositories
              .filter((repo) => !repo.hidden || repo.slug === os)
              .map((repo) => (
                <option value={repo.slug} key={repo.slug}>
                  {repo.name}
                </option>
              ))}
          </select>
        </div>
      </h1>
      <style jsx>{`
        .header {
          margin-bottom: 50px;
        }

        h1 {
          line-height: 1.4em;
        }

        .repo {
          display: inline-flex;
          align-items: center;
          transform: translateY(4px);
        }

        .repo :global(img) {
          width: 1em;
          height: 1em;
          display: inline;
          margin-left: 10px;
          margin-right: 5px;
        }

        select {
          font-weight: 600;
          border-radius: 0.25rem;
          background: transparent;
        }
      `}</style>
    </div>
  );
}
